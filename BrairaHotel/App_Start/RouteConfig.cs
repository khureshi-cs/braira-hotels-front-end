﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BrairaHotel
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "GalleryGroup",
               //url: "Gallery/{action}/{id}/{slug}",
               url: "Gallery/{name}",
               defaults: new { controller = "Home", action = "Gallery" },
               namespaces: new[] { "BrairaHotel.Controllers" }
           );

            routes.MapRoute(
               name: "GalleryGroup1",
               //url: "Gallery/{action}/{id}/{slug}",
               url: "Branches/{name}",
               defaults: new { controller = "Home", action = "Branches" },
               namespaces: new[] { "BrairaHotel.Controllers" }
           );

            routes.MapRoute(
              name: "NewsDetails",
              //url: "Gallery/{action}/{id}/{slug}",
              url: "News/{name}",
              defaults: new { controller = "News", action = "News" },
              namespaces: new[] { "BrairaHotel.Controllers" }
          );

            routes.MapRoute(
              name: "OffersDetails",
              url: "Offers/{name}",
              defaults: new { controller = "LatestOffers", action = "Offers" },
              namespaces: new[] { "BrairaHotel.Controllers" }
          );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "BrairaHotel.Controllers" }
            );
        }
    }
}
