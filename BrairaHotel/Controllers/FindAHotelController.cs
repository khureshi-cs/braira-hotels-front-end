﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Newtonsoft.Json;
using System.Data;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;
using BrairaHotel.Header;
using Boudl.Client.Mvc;
using BrairaHotel.Models;
using System.Text;

namespace BrairaHotel.Controllers
{
    public class FindAHotelController : BaseController
    {
        // GET: FindAHotel
        //public ActionResult Index()
        //{

        //    if(Session["HomeMenuList"]==null)
        //    {
        //        PrepareMenuData2();
        //    }
        //    String urlPrepend = System.Configuration.ConfigurationManager.AppSettings["UrlPrepend"].ToString();
        //    ViewData["urlPrepend"] = urlPrepend;
        //    return View();
        //}
        public static Int64 HtId = 0;
        public static string FixedAmenitiesIds = "";
        public async Task<ActionResult> Index()
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeader.setHeaders(client);
                ManageHotelAmenitiesDTO aObj = new ManageHotelAmenitiesDTO();
                FindAHotelNewDTO obj = new FindAHotelNewDTO();
                List<ManageCountriesDTO> CountriesList = new List<ManageCountriesDTO>();
                List<ManageCitiesDTO> Cityist = new List<ManageCitiesDTO>();
                string y = null;
                if (TempData["htlId"] != null)
                    ViewData["HotelId"] = TempData["htlId"];
                if (TempData["AmtId"] != null)
                    ViewData["AmenityId"] = TempData["AmtId"];
                List<ManageHotelAmenitiesDTO> a = new List<ManageHotelAmenitiesDTO>();
                ManageCountriesDTO CountryObj = new ManageCountriesDTO();
                ManageCitiesDTO cityObj = new ManageCitiesDTO();
                List<ManageCountriesDTO> CountryList = new List<ManageCountriesDTO>();
                List<ManageCitiesDTO> CityList = new List<ManageCitiesDTO>();
                List<ManageBranchesDTO> BranchList = new List<ManageBranchesDTO>();
                HttpResponseMessage AmenitiesRes = await client.PostAsJsonAsync("api/HotelAmenitiesAPI/NewGetHotelAmenity", aObj);
                if (AmenitiesRes.IsSuccessStatusCode)
                {
                    var AmenitiesData = AmenitiesRes.Content.ReadAsStringAsync().Result;
                    var AmenitiesList = JsonConvert.DeserializeObject<List<ManageHotelAmenitiesDTO>>(AmenitiesData);
                    a = AmenitiesList;
                    List<ManageHotelAmenitiesDTO> AmnList = AmenitiesList;
                    SelectList objModelData = new SelectList(AmnList, "Id", "NameEn", 0);
                    ViewBag.HotelAmenities = objModelData;
                }
                //HttpResponseMessage CountryRes = await client.PostAsJsonAsync("api/CountriesAPI/NewCountry", CountryObj);
                //if (CountryRes.IsSuccessStatusCode)
                //{
                //    var CountryData = CountryRes.Content.ReadAsStringAsync().Result;
                //    var ContryList = JsonConvert.DeserializeObject<List<ManageCountriesDTO>>(CountryData);
                //    SelectList ObjContData = new SelectList(ContryList, "Id", "NameEn", 0);
                //    ViewBag.CoutrList = ObjContData;
                //}
                obj.HotelId = 8;
                HttpResponseMessage AmenRes = await client.PostAsJsonAsync("api/FindAHotelAPI/NewMapData", obj);
                if (AmenRes.IsSuccessStatusCode)
                {
                    var resData = AmenRes.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ManageBranchesDTO>(resData);
                    var data = res.datasetXml;
                    if (data != null)
                    {
                        var document = new XmlDocument();
                        document.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(document));
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[3].Rows.Count > 0)
                            {
                                Cityist = ds.Tables[3].AsEnumerable().Select(dataRow => new ManageCitiesDTO
                                {
                                    Id = Convert.ToInt64(dataRow.Field<Int64>("CityId")),
                                    NameEn = dataRow.Field<string>("NameEn"),
                                    //NameAr = dataRow.Field<string>("NameAr"),
                                    countryName = dataRow.Field<string>("CountryEn")
                                }).ToList();
                                //TempData["CountriesList"] = CountriesList;
                                SelectList ObjContData = new SelectList(Cityist, "Id", "NameEn", "countryName", 0);
                                ViewBag.CoutrList = ObjContData;
                                Cityist = null;
                            }
                            else
                            {
                                SelectList objmodeldata = new SelectList("", 0);
                                ViewBag.CoutrList = objmodeldata;
                            }
                            if (TempData["htlId"] == null || TempData["AmtId"] == null)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    CountriesList = ds.Tables[0].AsEnumerable().Select(dataRow => new ManageCountriesDTO
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<Int64>("CountryId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    TempData["CountriesList"] = CountriesList;
                                }
                                else
                                {
                                    SelectList objmodeldata = new SelectList("", 0);
                                    ViewBag.cities = objmodeldata;
                                }

                                if (ds.Tables[1].Rows.Count > 0)
                                {
                                    Cityist = ds.Tables[1].AsEnumerable().Select(dataRow => new ManageCitiesDTO
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<Int64>("CityId")),
                                        CountryId = Convert.ToInt64(dataRow.Field<Int64>("CountryId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    TempData["Cityist"] = Cityist;
                                }
                                else
                                {
                                    SelectList objmodeldata = new SelectList("", 0);
                                    ViewBag.cities = objmodeldata;
                                }
                                if (ds.Tables[2].Rows.Count > 0)
                                {
                                    BranchList = ds.Tables[2].AsEnumerable().Select(dataRow => new ManageBranchesDTO
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<Int64>("Id")),
                                        CityId = Convert.ToInt64(dataRow.Field<Int64>("cityId")),
                                        HotelId = Convert.ToInt64(dataRow.Field<Int64>("HotelId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    TempData["BranchList"] = BranchList;
                                }
                                else
                                {
                                    SelectList objmodeldata = new SelectList("", 0);
                                    ViewBag.BranchList = objmodeldata;
                                }
                            }
                            TempData["htlId"] = HtId;
                        }
                    }
                }

                if (TempData["CountriesList"] != null)
                {
                    ViewBag.countries = TempData["CountriesList"];
                }
                if (TempData["Cityist"] != null)
                {
                    ViewBag.cities = TempData["Cityist"];
                }
                if (TempData["BranchList"] != null)
                {
                    ViewBag.BranchList = TempData["BranchList"];
                }
            }
            return View();
        }
        //public async Task<ActionResult> getAmenities(int[] AmenitiesIDs)
        public async Task<ActionResult> getAmenities(string AmenitiesIDs)
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeader.setHeaders(client);
                try
                {
                    bool status = false;
                    FindAHotelDTO a = new FindAHotelDTO();
                    List<FindAHotelDTO> aa = new List<FindAHotelDTO>();
                    List<FindAHotelDTO> ListofBranches = new List<FindAHotelDTO>();
                    a.Id = 8;
                    a.AmenityIds = FixedAmenitiesIds;
                    if (a.AmenityIds != "")
                    {
                        //a.AmenityIds = AmenitiesIDs == null ? "" : AmenitiesIDs.Remove(AmenitiesIDs.Length-1, 1);
                        StringBuilder sb = new StringBuilder(a.AmenityIds);
                        sb.Length--;
                        a.AmenityIds = sb.ToString();
                    }
                    HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/FindAHotelAPI/NewGetHotelsList", a);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var HotelsData = responseMessage.Content.ReadAsStringAsync().Result;
                        var HotelsList = JsonConvert.DeserializeObject<List<FindAHotelDTO>>(HotelsData);
                        ListofBranches = HotelsList;


                        return Json(HotelsList, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var result = "3";
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }

                    //return View("Error");
                }
                catch (Exception ex)
                {
                    ErrorLogDTO err = new ErrorLogDTO();

                    return RedirectToAction("Error");
                }
            }
        }
        [HttpGet]
        public async Task<ActionResult> GetMapByAmenity(string AmenitiesIDs, string latlongzoom)
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeader.setHeaders(client);
                try
                {
                    FixedAmenitiesIds = AmenitiesIDs;
                    bool status = false;
                    FindAHotelDTO a = new FindAHotelDTO();
                    //List<FindAHotelDTO> aa = new List<FindAHotelDTO>();
                    //List<FindAHotelDTO> ListofBranches = new List<FindAHotelDTO>();
                    a.Id = 1;
                    if (AmenitiesIDs != null || AmenitiesIDs == "")
                    {
                        a.AmenityIds = AmenitiesIDs;
                    }
                    else
                    {
                        a.Id = 0;
                    }
                    if (!string.IsNullOrEmpty(latlongzoom))
                    {
                        var Temppositions = latlongzoom;
                        Temppositions = latlongzoom.Replace("(", "");
                        Temppositions = Temppositions.Replace(")", "");
                        var positions = latlongzoom.Split(',');
                        if (positions[0] != null && positions[0].Length > 1)
                            a.Lat = positions[0];
                        else
                            a.Lat = "0";
                        if (positions[1] != null && positions[1].Length > 1)
                            a.Lng = positions[1];

                        else
                            a.Lng = "0";
                        if (positions.Length == 3)
                        {
                            if (positions[2] != null && positions[2].Length >= 1)
                                a.Zoom = positions[2];
                            else
                                a.Zoom = "0";
                        }
                        else
                            a.Zoom = "0";
                    }
                    else
                    {
                        a.Lat = "0";
                        a.Lng = "0";
                        a.Zoom = "0";
                    }

                    //HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/FindAHotelAPI/NewGetHotelsList", a);
                    //if (responseMessage.IsSuccessStatusCode)
                    //{
                    //    var HotelsData = responseMessage.Content.ReadAsStringAsync().Result;
                    //    var HotelsList = JsonConvert.DeserializeObject<List<FindAHotelDTO>>(HotelsData);
                    //    ListofBranches = HotelsList;
                    //    //a.listFindHotel = HotelsList;



                    //    var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    //    string outputOfFoos = serializer.Serialize(HotelsList);
                    //    JsonResult data= Json(HotelsList, JsonRequestBehavior.AllowGet);

                    //    a.JsonData = data;
                    //return Json(HotelsList, JsonRequestBehavior.AllowGet);
                    return View("GetMapByAmenity", a);

                    //}
                    //else
                    //{
                    //    //var result = "3";
                    //    return View("GetMapByAmenity", a);
                    //}

                    //return View("Error");
                }
                catch (Exception ex)
                {
                    ErrorLogDTO err = new ErrorLogDTO();

                    return RedirectToAction("Error");
                }
            }
        }
        public async Task<string> PrepareMenuData2()
        {
            using (HttpClient client = new HttpClient())
            {
                try
                {

                    CommonHeader.setHeaders(client);

                    List<ManageCitiesDTO> citylst = new List<ManageCitiesDTO>();
                    List<ManageBranchesDTO> branchList = new List<ManageBranchesDTO>();
                    List<ManageWeeklyOffersDTO> weeklyOffers = new List<ManageWeeklyOffersDTO>();
                    List<HomePageAboutSectionDTO> aboutList = new List<HomePageAboutSectionDTO>();
                    List<ManageCountriesDTO> countryList = new List<ManageCountriesDTO>();
                    List<HotelBannersDTO> bannersList = new List<HotelBannersDTO>();
                    List<CustomMenuDTO> HomeMenuList = new List<CustomMenuDTO>();
                    List<SpecialPackage> specialPackages = new List<SpecialPackage>();
                    List<HighlightsDTO> highlights = new List<HighlightsDTO>();
                    List<PageHeaderDTO> pageheaderlist = new List<PageHeaderDTO>();

                    ManageDistrictsDTO Obj = new ManageDistrictsDTO();
                    Obj.Id = 8;

                    HttpResponseMessage Ctres = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBookingService", Obj);

                    if (Ctres.IsSuccessStatusCode)
                    {
                        #region MenuPreparation

                        var CtData = Ctres.Content.ReadAsStringAsync().Result;
                        //=====================================================================
                        //var Ctlist = JsonConvert.DeserializeObject<List<object>>(CtData);
                        //List<object> objList = Ctlist;
                        //SelectList objModelData = new SelectList(objList, "Id", "NameEn", 0);
                        //ViewBag.CityList = objModelData;//BranchList
                        //=====================================================================
                        var categories = JsonConvert.DeserializeObject<ManageDistrictsDTO>(CtData);
                        var data = categories.datasetxml;
                        if (data != null)
                        {
                            var doc = new XmlDocument();
                            doc.LoadXml(data);
                            DataSet ds = new DataSet();
                            ds.ReadXml(new XmlNodeReader(doc));
                            if (ds.Tables.Count > 0)
                            {
                                //Hotel cities Binding
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    citylst = ds.Tables[0].AsEnumerable().Select(DataRow => new ManageCitiesDTO
                                    {
                                        Id = DataRow.Field<long>("Id"),
                                        CountryId = DataRow.Field<long>("CountryId"),
                                        NameEn = DataRow.Field<string>("NameEn"),
                                        NameAr = DataRow.Field<string>("NameAr")
                                    }).ToList();
                                    ViewBag.cityList = new SelectList(citylst, "Id", "NameEn", 0);
                                    Session["CitiesList"] = citylst;
                                }
                                else
                                {
                                    SelectList citlist = new SelectList("", 0);
                                    ViewBag.cityList = citlist;
                                }
                                // Hotel Branches Binding here
                                if (ds.Tables[1].Rows.Count > 0)
                                {
                                    branchList = ds.Tables[1].AsEnumerable().Select(DataRow => new ManageBranchesDTO
                                    {
                                        Id = DataRow.Field<Int64>("Id"),
                                        BookingUrl = DataRow.Field<string>("BookingUrl"),
                                        CityId = DataRow.Field<long>("CityId"),
                                        NameEn = DataRow.Field<string>("NameEn"),
                                        NameAr = DataRow.Field<string>("NameAr")
                                    }).ToList();
                                    //ViewBag.BrnList = new SelectList(branchList, "BookingUrl", "NameEn", 0);
                                    ViewBag.BrnList1 = new SelectList(branchList);
                                    //ViewBag.BranchesList = branchList;
                                    Session["BranchesList"] = branchList;
                                    ViewBag.BrnList = JsonConvert.SerializeObject(branchList);
                                }
                                else
                                {
                                    SelectList Branchlist = new SelectList("", 0);
                                    ViewBag.BrnList = Branchlist;
                                }

                                if (ds.Tables[7].Rows.Count > 0)
                                {
                                    countryList = ds.Tables[7].AsEnumerable().Select(DataRow => new ManageCountriesDTO
                                    {
                                        Id = DataRow.Field<long>("Id"),
                                        NameEn = DataRow.Field<string>("NameEn"),
                                        NameAr = DataRow.Field<string>("NameAr"),
                                    }).ToList();
                                    ViewBag.CountriesList = countryList;
                                    Session["CountriesList"] = countryList;
                                }
                                else
                                {
                                    SelectList CountryList1 = new SelectList("", 0);
                                    ViewBag.CountriesList = CountryList1;
                                }
                                if (ds.Tables[8].Rows.Count > 0)
                                {
                                    HomeMenuList = ds.Tables[8].AsEnumerable().Select(DataRow => new CustomMenuDTO
                                    {
                                        Id = DataRow.Field<Int64>("Id"),
                                        NameEn = DataRow.Field<string>("NameEn"),
                                        NameAr = DataRow.Field<string>("NameAr"),
                                        DynamicBranches = DataRow.Field<bool>("DynamicBranches"),
                                        URL = DataRow.Field<string>("URL"),
                                        SubUrl = DataRow.Field<string>("SubUrl"),
                                        UrlAr = DataRow.Field<string>("UrlAr"),
                                        SubUrlAr = DataRow.Field<string>("SubUrlAr"),
                                        SortIndex = DataRow.Field<int>("SortIndex")
                                    }).ToList();
                                    ViewBag.HomeMenuList = HomeMenuList;
                                    Session["HomeMenuList"] = HomeMenuList;
                                }
                                else
                                {
                                    SelectList HomeMenuList1 = new SelectList("", 0);
                                    ViewBag.HomeMenuList = HomeMenuList1;
                                }

                                //Page Header Banners
                                if (ds.Tables[15].Rows.Count > 0)
                                {
                                    pageheaderlist = ds.Tables[15].AsEnumerable().Select(DataRow => new PageHeaderDTO
                                    {
                                        HotelId = DataRow.Field<long>("HotelId"),
                                        CustomerMenuId = DataRow.Field<long>("CustomerMenuId"),
                                        MenuNameEn = DataRow.Field<string>("MenuNameEn"),
                                        MenuNameAr = DataRow.Field<string>("MenuNameAr"),
                                        ImagePath = DataRow.Field<string>("ImagePath"),
                                    })
                                    //.Where(x => x.MenuNameEn == "ABOUTUS")
                                    .ToList();
                                    //ViewBag.PageHeader = pageheaderlist;
                                    System.Web.HttpContext.Current.Session["PageHeader"] = pageheaderlist;
                                }
                                else
                                {
                                    System.Web.HttpContext.Current.Session["PageHeader"] = null;
                                }

                            }
                        }
                        #endregion
                    }


                }
                catch (Exception Ex)
                {

                    throw;
                }

            }

            return "";

        }
        public async Task<ActionResult> getData(string HtId, string para)
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeader.setHeaders(client);
                try
                {
                    List<ManageCountriesDTO> CountriesList = new List<ManageCountriesDTO>();
                    List<ManageCitiesDTO> Cityist = new List<ManageCitiesDTO>();
                    List<ManageBranchesDTO> BranchList = new List<ManageBranchesDTO>();
                    FindAHotelNewDTO obj = new FindAHotelNewDTO();
                    if (string.IsNullOrEmpty(HtId))
                    {
                        obj.CountryId = 0;
                    }
                    else
                        obj.CountryId = Convert.ToInt64(HtId);

                    if (string.IsNullOrEmpty(para))
                    {
                        obj.Id = 0;
                    }
                    else
                        obj.Id = Convert.ToInt64(para);
                    obj.HotelId = 8;
                    TempData["AmtId"] = para;
                    //HtId = obj.HotelId;
                    HttpResponseMessage AmenRes = await client.PostAsJsonAsync("api/FindAHotelAPI/NewMapData", obj);
                    if (AmenRes.IsSuccessStatusCode)
                    {
                        var resData = AmenRes.Content.ReadAsStringAsync().Result;
                        var res = JsonConvert.DeserializeObject<ManageBranchesDTO>(resData);
                        var data = res.datasetXml;
                        if (data != null)
                        {
                            var document = new XmlDocument();
                            document.LoadXml(data);
                            DataSet ds = new DataSet();
                            ds.ReadXml(new XmlNodeReader(document));
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    CountriesList = ds.Tables[0].AsEnumerable().Select(dataRow => new ManageCountriesDTO
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<Int64>("CountryId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        //NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    TempData["CountriesList"] = CountriesList;
                                }
                                else
                                {
                                    SelectList objmodeldata = new SelectList("", 0);
                                    ViewBag.cities = objmodeldata;
                                }
                                if (ds.Tables[1].Rows.Count > 0)
                                {
                                    Cityist = ds.Tables[1].AsEnumerable().Select(dataRow => new ManageCitiesDTO
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<Int64>("CityId")),
                                        CountryId = Convert.ToInt64(dataRow.Field<Int64>("CountryId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    TempData["Cityist"] = Cityist;
                                }
                                else
                                {
                                    SelectList objmodeldata = new SelectList("", 0);
                                    ViewBag.cities = objmodeldata;
                                }
                                if (ds.Tables[2].Rows.Count > 0)
                                {
                                    BranchList = ds.Tables[2].AsEnumerable().Select(dataRow => new ManageBranchesDTO
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<Int64>("Id")),
                                        CityId = Convert.ToInt64(dataRow.Field<Int64>("cityId")),
                                        HotelId = Convert.ToInt64(dataRow.Field<Int64>("HotelId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    TempData["BranchList"] = BranchList;
                                }
                                else
                                {
                                    SelectList objmodeldata = new SelectList("", 0);
                                    ViewBag.BranchList = objmodeldata;
                                }
                            }
                            TempData["htlId"] = HtId;
                        }
                    }
                    return RedirectToAction("Index");
                    //return View();
                    //return Json(BranchList, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ErrorLogDTO err = new ErrorLogDTO();
                    return RedirectToAction("Error");
                }
            }
        }
        [HttpGet]
        public async Task<ActionResult> GetMapData(string HtId, string para)
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeader.setHeaders(client);
                try
                {
                    //ManageHotelAmenitiesDTO aObj = new ManageHotelAmenitiesDTO();
                    //List<ManageHotelAmenitiesDTO> a = new List<ManageHotelAmenitiesDTO>();
                    //ManageCountriesDTO CountryObj = new ManageCountriesDTO();
                    //ManageCitiesDTO cityObj = new ManageCitiesDTO();
                    //List<ManageCountriesDTO> CountryList = new List<ManageCountriesDTO>();
                    //List<ManageCitiesDTO> CityList = new List<ManageCitiesDTO>();
                    ////List<ManageBranchesDTO> BranchList = new List<ManageBranchesDTO>();
                    //HttpResponseMessage AmenitiesRes = await client.PostAsJsonAsync("api/HotelAmenitiesAPI/NewGetHotelAmenity", aObj);
                    //if (AmenitiesRes.IsSuccessStatusCode)
                    //{
                    //    var AmenitiesData = AmenitiesRes.Content.ReadAsStringAsync().Result;
                    //    var AmenitiesList = JsonConvert.DeserializeObject<List<ManageHotelAmenitiesDTO>>(AmenitiesData);
                    //    a = AmenitiesList;
                    //    List<ManageHotelAmenitiesDTO> AmnList = AmenitiesList;
                    //    SelectList objModelData = new SelectList(AmnList, "Id", "NameEn", 0);
                    //    ViewBag.HotelAmenities = objModelData;
                    //}
                    //HttpResponseMessage CountryRes = await client.PostAsJsonAsync("api/CountriesAPI/NewCountry", CountryObj);
                    //if (CountryRes.IsSuccessStatusCode)
                    //{
                    //    var CountryData = CountryRes.Content.ReadAsStringAsync().Result;
                    //    var ContryList = JsonConvert.DeserializeObject<List<ManageCountriesDTO>>(CountryData);
                    //    SelectList ObjContData = new SelectList(ContryList, "Id", "NameEn", 0);
                    //    ViewBag.CoutrList = ObjContData;
                    //}
                    List<ManageCountriesDTO> CountriesList = new List<ManageCountriesDTO>();
                    List<ManageCitiesDTO> Cityist = new List<ManageCitiesDTO>();
                    List<ManageBranchesDTO> BranchList = new List<ManageBranchesDTO>();
                    FindAHotelNewDTO obj = new FindAHotelNewDTO();
                    if (string.IsNullOrEmpty(HtId))
                    {
                        //obj.CountryId = 0;
                        obj.CityId = 0;
                    }
                    else
                        //obj.CountryId = Convert.ToInt64(HtId);
                        obj.CityId = Convert.ToInt64(HtId);


                    //if (string.IsNullOrEmpty(para))
                    //{
                    //    obj.Id = 0;
                    //}
                    //else
                    //    obj.Id = Convert.ToInt64(para);

                    if (!string.IsNullOrEmpty(para))
                        obj.AmenityIds = para.Remove(para.Length - 1);

                    obj.HotelId = 8;
                    //TempData["AmtId"] = para;
                    //HtId = obj.HotelId;;
                    HttpResponseMessage AmenRes = await client.PostAsJsonAsync("api/FindAHotelAPI/NewMapData", obj);
                    if (AmenRes.IsSuccessStatusCode)
                    {
                        var resData = AmenRes.Content.ReadAsStringAsync().Result;
                        var res = JsonConvert.DeserializeObject<ManageBranchesDTO>(resData);
                        var data = res.datasetXml;
                        if (data != null)
                        {
                            var document = new XmlDocument();
                            document.LoadXml(data);
                            DataSet ds = new DataSet();
                            ds.ReadXml(new XmlNodeReader(document));
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    CountriesList = ds.Tables[0].AsEnumerable().Select(dataRow => new ManageCountriesDTO
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<Int64>("CountryId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        //NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    ViewBag.countries = CountriesList;
                                    //ViewData["CountriesList"] = CountriesList;
                                }
                                else
                                {
                                    SelectList objmodeldata = new SelectList("", 0);
                                    ViewBag.countries = null;
                                }
                                if (ds.Tables[1].Rows.Count > 0)
                                {
                                    Cityist = ds.Tables[1].AsEnumerable().Select(dataRow => new ManageCitiesDTO
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<Int64>("CityId")),
                                        CountryId = Convert.ToInt64(dataRow.Field<Int64>("CountryId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    ViewBag.cities = Cityist;
                                    //ViewBag.cities = Cityist;
                                    //ViewData["Cityist"] = Cityist;
                                }
                                else
                                {
                                    //SelectList objmodeldata = new SelectList("", 0);
                                    ViewBag.cities = null;
                                }
                                if (ds.Tables[2].Rows.Count > 0)
                                {
                                    BranchList = ds.Tables[2].AsEnumerable().Select(dataRow => new ManageBranchesDTO
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<Int64>("Id")),
                                        CityId = Convert.ToInt64(dataRow.Field<Int64>("cityId")),
                                        HotelId = Convert.ToInt64(dataRow.Field<Int64>("HotelId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    ViewBag.BranchList = BranchList;
                                    //ViewData["BranchList"] = BranchList;
                                }
                                else
                                {
                                    //SelectList objmodeldata = new SelectList("", 0);
                                    ViewBag.BranchList = null;
                                }
                            }
                            //TempData["htlId"] = HtId;
                        }
                    }
                    FindAHotelDTO outobj = new FindAHotelDTO();
                    //outobj.CityEn = "TempCity";
                    return View("GetMapData", outobj);
                    //return View();
                    //return Json(BranchList, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    //ErrorLogDTO err = new ErrorLogDTO();
                    //return RedirectToAction("Error");
                    FindAHotelDTO outobj = new FindAHotelDTO();
                    //outobj.CityEn = "TempCity";
                    return View("GetMapData", outobj);
                }
            }
        }
    }
}