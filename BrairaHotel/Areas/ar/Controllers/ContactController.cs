﻿using BrairaHotel.Header;
using BrairaHotel.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace BrairaHotel.Areas.ar.Controllers
{
    public class ContactController : BaseController
    {
        public static string succss = string.Empty;
        public static string Error = string.Empty;
        // GET: Contact
        public ActionResult Index()
        {
            if (Session["PageHeader"] != null)
            {
                List<PageHeaderDTO> pageHeaders = ((List<PageHeaderDTO>)Session["PageHeader"]).ToList();
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                PageHeaderDTO pageHeader = pageHeaders.Where(x => x.MenuNameEn.ToLower() == "contactus").FirstOrDefault();
                if (pageHeader != null)
                {
                    ViewBag.PageHeaderBanner = img + pageHeader.ImagePath;
                    ViewBag.PageHeaderClass = "";
                }
                else
                {
                    ViewBag.PageHeaderBanner = "";
                    ViewBag.PageHeaderClass = " gallery-header";
                }
            }
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Index(ContactRequestDTO obj)
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeader.setHeaders(client);
                try
                {
                    //ContactRequestDTO ContObj = new ContactRequestDTO();
                    //ContObj.HotelId = 7;
                    obj.HotelId = 8;
                    sendMail(obj);
                    //if()
                    HttpResponseMessage CtReq = await client.PostAsJsonAsync("api/ContactAPI/AddContact", obj);
                    if (CtReq.IsSuccessStatusCode)
                    {
                        var responseData = CtReq.Content.ReadAsStringAsync().Result;
                        var res = JsonConvert.DeserializeObject<ContactRequestDTO>(responseData);
                        string msg = res.MsgStatus;
                        if (msg == "1")
                        {
                            succss = "Contact Request Saved Successfully";
                            ViewData["Success"] = succss.ToString();
                        }


                        else if (msg == "4")
                        {
                            TempData["ErrorData"] = obj;
                            Error = "Failed to Save Contact Request";
                            ViewData["Error"] = Error.ToString();
                        }
                    }
                    else
                    {
                        TempData["ErrorData"] = obj;
                        Error = "Failed to Insert/Update Contact Request";
                    }
                    if (Session["PageHeader"] != null)
                    {
                        List<PageHeaderDTO> pageHeaders = ((List<PageHeaderDTO>)Session["PageHeader"]).ToList();
                        string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                        PageHeaderDTO pageHeader = pageHeaders.Where(x => x.MenuNameEn.ToLower() == "contactus").FirstOrDefault();
                        if (pageHeader != null)
                        {
                            ViewBag.PageHeaderBanner = img + pageHeader.ImagePath;
                            ViewBag.PageHeaderClass = "";
                        }
                        else
                        {
                            ViewBag.PageHeaderBanner = "";
                            ViewBag.PageHeaderClass = " gallery-header";
                        }
                    }
                    return View();
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return RedirectToAction("Index", "Contact");
                }
            }
            //return View();
        }
        private void sendMail(ContactRequestDTO _Contact)
        {
            try
            {
                string mailTo = _Contact.Email;
                string mailfrom = System.Configuration.ConfigurationManager.AppSettings["mailFrom"].ToString();

                string mailBCC = string.Empty;
                string logoPath = "http://csadms.com/BoudlGroupTest/Images/Boudlhome.png ";//System.Configuration.ConfigurationManager.AppSettings["mailLogo"].ToString();

                string URL = "https://csadms.com/BoudlGroupTest";//System.Configuration.ConfigurationManager.AppSettings["URL"].ToString();

                string Subject = "Contact Us Query";

                string strHTML = @"<html><body><div align='center' style='width: 570px; font-family: Verdana, Geneva, sans-serif;
                                border: 1px solid #9C0606   display: block;'>
                                <table style='width: 570px; width: 51px; height: 28px; margin: 0px; border: 1px solid #9C0606'>
                                <tr><td><table style='margin-left: 0px; width: 570px;'><tr><td align='left'><a href='" + URL + @"' target='_blank'>
                                <img src='" + logoPath + @"' alt='Globus Logistics' width='240px' height='120px' style='border: 0px' />
                                </a></td></tr></table></td></tr><tr><td><hr /></td></tr><tr><td>
                                <table border='0px' cellpadding='0px' cellspacing='0px' style='width: 570px;'>
                                <tr style='height: 10px;'><td></td></tr><tr><td align='left'>
                                <font face='Verdana' style='font-size: 11px'>Dear <b>Admin</b>,</font>
                                </td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='left'>
                                <font face='Verdana' style='font-size: 11px'>The following customer submitted a query from the website</font>
                                </td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='left'><p align='justify'>
                                <font face='Verdana' style='font-size: 11px'><strong>Name:</strong> " + _Contact.FullName + @"<br />
                                <strong>Email:</strong> " + _Contact.Email + @" <br /><strong>Mobile:</strong> " + _Contact.Phone + @"<br />
                                <strong>Comment:</strong> <br />" + _Contact.Message + @" </font></p></td></tr><tr><td style='height: 8px;'>
                                </td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='left'>
                                <p><font face='Verdana' style='font-size: 11px'>
                                As this is an automated response, please do not reply to this email.</font>
                                </p></td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='right'>
                                <font face='Verdana' style='font-size: 11px'>Info Team</font>
                                </td></tr><tr style='height: 10px;'><td></td></tr></table></td></tr></table>
                                <br /></div></body></html>";
                string ClientHTML = @"<html><body><div align='center' style='width: 570px; font-family: Verdana, Geneva, sans-serif;
                                border: 1px solid #9C0606   display: block;'>
                                <table style='width: 570px; width: 51px; height: 28px; margin: 0px; border: 1px solid #9C0606'>
                                <tr><td><table style='margin-left: 0px; width: 570px;'><tr><td align='left'><a href='" + URL + @"' target='_blank'>
                                <img src='" + logoPath + @"' alt='Globus Logistics' width='240px' height='120px' style='border: 0px' />
                                </a></td></tr></table></td></tr><tr><td><hr /></td></tr><tr><td>
                                <table border='0px' cellpadding='0px' cellspacing='0px' style='width: 570px;'>
                                <tr style='height: 10px;'><td></td></tr><tr><td align='left'>
                                <font face='Verdana' style='font-size: 11px'>Dear <strong>" + _Contact.FullName + @"</strong>,</font>
                                </td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='left'>
                                <font face='Verdana' style='font-size: 11px'>Your Request Query has been placed successfully, Our Executive will contact you Shortly.</font>
                                </td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='left'><p align='justify'>
                                <font face='Verdana' style='font-size: 11px'><strong>Name:</strong> " + _Contact.FullName + @"<br />
                                <strong>Email:</strong> " + _Contact.Email + @" <br /><strong>Mobile:</strong> " + _Contact.Phone + @"<br />
                                <strong>Comment:</strong> <br />" + _Contact.Message + @" </font></p></td></tr><tr><td style='height: 8px;'>
                                </td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='left'>
                                <p><font face='Verdana' style='font-size: 11px'>
                                As this is an automated response, please do not reply to this email.</font>
                                </p></td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='right'>
                                <font face='Verdana' style='font-size: 11px'>Info Team</font>
                                </td></tr><tr style='height: 10px;'><td></td></tr></table></td></tr></table>
                                <br /></div></body></html>";
                Common.sendEmail(mailfrom, mailTo, strHTML, ClientHTML, Subject, mailBCC);
            }
            catch (Exception Ex)
            {
                string msg = Ex.Message;
                throw;
            }
        }
    }
}