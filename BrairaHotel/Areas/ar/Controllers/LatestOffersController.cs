﻿using BrairaHotel.Header;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace BrairaHotel.Areas.ar.Controllers
{
    public class LatestOffersController : BaseController
    {
        public async Task<ActionResult> Index()
        {

            //Prepare Data
            using (HttpClient client = new HttpClient())
            {

                CommonHeader.setHeaders(client);
                ManageNewsDTO Obj = new ManageNewsDTO();
                Obj.Id = 8;

                Obj.NameEn = "null";
                ManageDistrictsDTO Obj1 = new ManageDistrictsDTO();
                Obj1.Id = 8;
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                List<SpecialPackage> splPackage = new List<SpecialPackage>();
                List<PageHeaderDTO> pageheaderlist = new List<PageHeaderDTO>();
                HttpResponseMessage Ctres = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBookingService", Obj1);
                HttpResponseMessage splPkgServ = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBannerService", Obj);
                if (splPkgServ.IsSuccessStatusCode)
                {
                    var NewsData = splPkgServ.Content.ReadAsStringAsync().Result;
                    var News = JsonConvert.DeserializeObject<ManageNewsDTO>(NewsData);
                    var data = News.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[1].Rows.Count > 0)
                            {
                                splPackage = ds.Tables[1].AsEnumerable().Select(dr => new SpecialPackage
                                {
                                    Id = dr.Field<Int64>("Id"),
                                    HotelId = dr.Field<Int64>("HotelId"),
                                    BranchId = dr.Field<Int64>("BranchId"),
                                    bookingURL = dr.Field<String>("bookingURL"),
                                    NameEn = dr.Field<String>("NameEn"),
                                    NameAr = dr.Field<String>("NameAr"),
                                    DescriptionEn = dr.Field<String>("DescriptionEn"),
                                    DescriptionAr = dr.Field<String>("DescriptionAr"),
                                    ImageEn = img + dr.Field<String>("ImageEn"),
                                    ImageAr = img + dr.Field<String>("ImageAr"),
                                    StartDate = dr.Field<DateTime>("StartDate"),
                                    EndDate = dr.Field<DateTime>("EndDate"),
                                    SortIndex = dr.Field<Int32>("SortIndex"),
                                    BookingRedirection = dr.Field<bool>("BookingRedirection"),
                                    AmenitiesEn = dr.Field<String>("AmenitiesEn"),
                                    AmenitiesAr = dr.Field<String>("AmenitiesAr"),
                                    
                                    OfferType = dr.Field<Int32>("OfferType"),
                                    Mode = dr.Field<Int32>("Mode"),
                                    OfferPrice = dr.Field<decimal>("OfferPrice"),
                                    Currency = dr.Field<String>("Currency"),
                                    arAmnEn = dr.Field<String>("AmenitiesEn").Split(','),
                                    arAmnAr = dr.Field<String>("AmenitiesAr").Split(','),
                                    amIcons = dr.Field<String>("IconClass").Split(',')
                                }).ToList();
                                ViewBag.splPkgs = splPackage;
                            }
                            else
                            {
                                SelectList newslist = new SelectList("", 0);
                                ViewBag.splPkgs = newslist;
                            }
                        }
                    }
                }
                if (Ctres.IsSuccessStatusCode)
                {
                    #region Page Header Banner

                    var CtData = Ctres.Content.ReadAsStringAsync().Result;

                    var categories = JsonConvert.DeserializeObject<ManageDistrictsDTO>(CtData);
                    var data = categories.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {

                            //Page Header Banners
                            if (ds.Tables[15].Rows.Count > 0)
                            {
                                pageheaderlist = ds.Tables[15].AsEnumerable().Select(DataRow => new PageHeaderDTO
                                {
                                    HotelId = DataRow.Field<long>("HotelId"),
                                    CustomerMenuId = DataRow.Field<long>("CustomerMenuId"),
                                    MenuNameEn = DataRow.Field<string>("MenuNameEn"),
                                    MenuNameAr = DataRow.Field<string>("MenuNameAr"),
                                    ImagePath = DataRow.Field<string>("ImagePath"),
                                })
                                .Where(x => x.MenuNameEn == "OFFERS")
                                .ToList();
                                //ViewBag.PageHeader = pageheaderlist;
                                ViewBag.PageHeaderBanner = img + pageheaderlist.Select(x => x.ImagePath).FirstOrDefault();
                                ViewBag.PageHeaderClass = "";
                                //System.Web.HttpContext.Current.Session["PageHeader"] = pageheaderlist;
                            }
                            else
                            {
                                ViewBag.PageHeaderBanner = "";
                                ViewBag.PageHeaderClass = " gallery-header";
                                //System.Web.HttpContext.Current.Session["PageHeader"] = null;
                            }
                        }
                    }
                    #endregion
                }
            }
            if (Session["PageHeader"] != null)
            {
                List<PageHeaderDTO> pageHeaders = ((List<PageHeaderDTO>)Session["PageHeader"]).ToList();
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                PageHeaderDTO pageHeader = pageHeaders.Where(x => x.MenuNameEn.ToLower() == "offers").FirstOrDefault();
                if (pageHeader != null)
                {
                    ViewBag.PageHeaderBanner1 = img + pageHeader.ImagePath;
                    ViewBag.PageHeaderClass = "";
                }
                else
                {
                    ViewBag.PageHeaderBanner1 = "";
                    ViewBag.PageHeaderClass = " gallery-header";
                }
            }
            return View();

        }
        [Route("Offers")]
        public async Task<ActionResult> Offers(string name)
        {
            using (HttpClient client = new HttpClient())
            {

                CommonHeader.setHeaders(client);
                ManageOffersDTO Obj = new ManageOffersDTO();
                Obj.Id = 8;
                Obj.NameEn = name;
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                List<ManageOffersDTO> OffersDetails = new List<ManageOffersDTO>();
                HttpResponseMessage OffersRes = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/OffersDetailService", Obj);
                if (OffersRes.IsSuccessStatusCode)
                {
                    var bnrData = OffersRes.Content.ReadAsStringAsync().Result;
                    var banners = JsonConvert.DeserializeObject<ManageOffersDTO>(bnrData);
                    var data = banners.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                OffersDetails = ds.Tables[0].AsEnumerable().Select(DataRow => new ManageOffersDTO
                                {
                                    Id = DataRow.Field<long>("Id"),
                                    NameEn = DataRow.Field<string>("NameEn"),
                                    NameAr = DataRow.Field<string>("NameAr"),
                                    DescriptionEn = DataRow.Field<string>("DescriptionEn"),
                                    DescriptionAr = DataRow.Field<string>("DescriptionAr"),
                                    //CreatedOn = DataRow.Field<DateTime>("CreatedOn"),
                                    BookingRedirection = DataRow.Field<bool>("BookingRedirection"),
                                    OfferBookingUrl = DataRow.Field<string>("OfferBookingUrl"),
                                    ImageEn = img + "/" + DataRow.Field<string>("ImageEn"),
                                    ImageAr = img + "/" + DataRow.Field<string>("ImageAr"),
                                    arAmnEn = DataRow.Field<String>("AmenitiesEn").Split(','),
                                    arAmnAr = DataRow.Field<String>("AmenitiesAr").Split(','),
                                    amIcons = DataRow.Field<String>("IconClass").Split(','),
                                    Mode = DataRow.Field<Int32>("Mode"),
                                    OfferPrice = DataRow.Field<decimal>("OfferPrice"),
                                    Currency = DataRow.Field<String>("Currency")
                                }).ToList();
                                //ViewBag.BookingUrl= ds.Tables[0].AsEnumerable().Select(DataRow => new ManageOffersDTO
                                //{
                                //    BookingUrl = DataRow.Field<string>("BookingUrl"),
                                //}).FirstOrDefault().BookingUrl;
                                ViewBag.BookingRedirection = OffersDetails.FirstOrDefault().BookingRedirection;
                                ViewBag.BookingUrl = OffersDetails.FirstOrDefault().OfferBookingUrl;
                                ViewBag.Mode = OffersDetails.FirstOrDefault().Mode;
                                ViewBag.OfferPrice = OffersDetails.FirstOrDefault().OfferPrice;
                                ViewBag.OffersDetail = OffersDetails;
                            }
                            else
                            {
                                SelectList bnrlist = new SelectList("", 0);
                                ViewBag.OffersDetail = bnrlist;
                            }
                        }
                    }
                }
                List<PageHeaderDTO> pageheaderlist = new List<PageHeaderDTO>();
                ManageDistrictsDTO Obj1 = new ManageDistrictsDTO();
                Obj1.Id = 8;
                HttpResponseMessage Ctres = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBookingService", Obj1);
                if (Ctres.IsSuccessStatusCode)
                {
                    #region Page Header Banner

                    var CtData = Ctres.Content.ReadAsStringAsync().Result;

                    var categories = JsonConvert.DeserializeObject<ManageDistrictsDTO>(CtData);
                    var data = categories.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {

                            //Page Header Banners
                            if (ds.Tables[15].Rows.Count > 0)
                            {
                                pageheaderlist = ds.Tables[15].AsEnumerable().Select(DataRow => new PageHeaderDTO
                                {
                                    HotelId = DataRow.Field<long>("HotelId"),
                                    CustomerMenuId = DataRow.Field<long>("CustomerMenuId"),
                                    MenuNameEn = DataRow.Field<string>("MenuNameEn"),
                                    MenuNameAr = DataRow.Field<string>("MenuNameAr"),
                                    ImagePath = DataRow.Field<string>("ImagePath"),
                                })
                                .Where(x => x.MenuNameEn.ToLower() == "offers")
                                .ToList();
                                //ViewBag.PageHeader = pageheaderlist;
                                ViewBag.PageHeaderBanner = img + pageheaderlist.Select(x => x.ImagePath).FirstOrDefault();
                                ViewBag.PageHeaderClass = "";
                                //System.Web.HttpContext.Current.Session["PageHeader"] = pageheaderlist;
                            }
                            else
                            {
                                ViewBag.PageHeaderBanner = "";
                                ViewBag.PageHeaderClass = " gallery-header";
                                //System.Web.HttpContext.Current.Session["PageHeader"] = null;
                            }
                        }
                    }
                    #endregion
                }
            }
            return View();
        }
    }
}