﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace BrairaHotel.Utility
{
    public class Common
    {

        public static bool sendEmail(string mailfrom, string mailTo, string strHTML, string ClientHTML, string Subject, string mailBCC)
        {
            try
            {
                string msg1 = string.Empty;
                MailMessage mMailMessage = new MailMessage();
                mMailMessage.From = new MailAddress(mailfrom);
                List<string> ToAddress = new List<string>();

                ToAddress.Add(System.Configuration.ConfigurationManager.AppSettings["admin"].ToString());
                ToAddress.Add(mailTo);
                foreach (var toAdr in ToAddress)
                {
                    if (toAdr != System.Configuration.ConfigurationManager.AppSettings["admin"].ToString())
                    {
                        mMailMessage.To.Add(new MailAddress(toAdr));
                        if (!string.IsNullOrWhiteSpace(mailBCC))
                            mMailMessage.Bcc.Add(mailBCC);

                        mMailMessage.Subject = Subject;
                        mMailMessage.Body = ClientHTML;
                        mMailMessage.IsBodyHtml = true;
                        mMailMessage.Priority = MailPriority.Normal;

                        SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SMTPServer"], Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["smtpPort"]));

                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["mailFrom"].ToString(), System.Configuration.ConfigurationManager.AppSettings["mailCredentialPassword"].ToString());

                        //SmtpClient mSmtpClient = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SMTPServer"],
                        //Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["smtpPort"]));
                        //mSmtpClient.UseDefaultCredentials = false;
                        //mSmtpClient.Port = 587;
                        //mSmtpClient.EnableSsl = true;
                        //mSmtpClient.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["mailFrom"].ToString(),
                        //System.Configuration.ConfigurationManager.AppSettings["mailCredentialPassword"].ToString());
                        smtp.Send(mMailMessage);
                    }
                    else
                    {
                        mMailMessage.To.Add(new MailAddress(toAdr));
                        if (!string.IsNullOrWhiteSpace(mailBCC))
                            mMailMessage.Bcc.Add(mailBCC);

                        mMailMessage.Subject = Subject;
                        mMailMessage.Body = strHTML;
                        mMailMessage.IsBodyHtml = true;
                        mMailMessage.Priority = MailPriority.Normal;

                        SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SMTPServer"], Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["smtpPort"]));
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["mailFrom"].ToString(), System.Configuration.ConfigurationManager.AppSettings["mailCredentialPassword"].ToString());
                        smtp.Send(mMailMessage);
                    }

                }
                return true;
            }
            catch (Exception Ex)
            {
                string msg = Ex.Message;
                return false;
            }
        }
        //public static bool sendEmail(string mailfrom, string mailTo, string strHTML, string Subject, string mailBCC, bool isSSL, List<LinkedResource> lstLinkedResource, List<System.Net.Mail.Attachment> lstAttachments)
        //{
        //    try
        //    {
        //        string msg1 = string.Empty;
        //        MailMessage mMailMessage = new MailMessage();
        //        mMailMessage.From = new MailAddress(mailfrom);
        //        mMailMessage.To.Add(new MailAddress(mailTo));

        //        if (!string.IsNullOrWhiteSpace(mailBCC))
        //            mMailMessage.Bcc.Add(mailBCC);

        //        mMailMessage.Subject = Subject;
        //        mMailMessage.SubjectEncoding = Encoding.UTF8;
        //        mMailMessage.BodyEncoding = Encoding.UTF8;
        //        //mMailMessage.Body = strHTML;
        //        AlternateView mAlternateView = AlternateView.CreateAlternateViewFromString(strHTML, Encoding.UTF8, System.Net.Mime.MediaTypeNames.Text.Html);
        //        mAlternateView.ContentType = new System.Net.Mime.ContentType("text/html");
        //        mAlternateView.ContentId = "htmlView";
        //        mAlternateView.TransferEncoding = System.Net.Mime.TransferEncoding.SevenBit;

        //        if (lstLinkedResource != null)
        //            for (int i = 0; i < lstLinkedResource.Count; i++)
        //                mAlternateView.LinkedResources.Add(lstLinkedResource[i]);

        //        if (lstAttachments != null)
        //            for (int i = 0; i < lstAttachments.Count; i++)
        //                mMailMessage.Attachments.Add(lstAttachments[i]);

        //        mMailMessage.AlternateViews.Add(mAlternateView);

        //        mMailMessage.IsBodyHtml = true;
        //        mMailMessage.Priority = MailPriority.Normal;
        //        SmtpClient mSmtpClient = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SMTPServer"],
        //                                    Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PortNo"]));

        //        #region For SSL
        //        if (isSSL)
        //        {
        //            mSmtpClient.EnableSsl = true;
        //            mSmtpClient.TargetName = System.Configuration.ConfigurationManager.AppSettings["TargetName"]; //"STARTTLS/smtp.office365.com";
        //        }
        //        #endregion
        //        mSmtpClient.UseDefaultCredentials = false;
        //        mSmtpClient.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["mailFrom"].ToString(),
        //        System.Configuration.ConfigurationManager.AppSettings["mailCredentialPassword"].ToString());
        //        mSmtpClient.Send(mMailMessage);
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}
    }
}