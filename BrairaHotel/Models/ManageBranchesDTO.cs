﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrairaHotel
{
    public class ManageBranchesDTO
    {
        public Int64 Id { get; set; }
        [Required(ErrorMessage = "*Select Hotel Name", AllowEmptyStrings = false)]
        public Int64 HotelId { get; set; }
        public string HotelNameEn { get; set; }
        [Required(ErrorMessage = "*Select Country Name", AllowEmptyStrings = false)]
        public Int64 CountryId { get; set; }
        [Required(ErrorMessage = "*Select City Name", AllowEmptyStrings = false)]
        public Int64 CityId { get; set; }
        [Required(ErrorMessage = "*Select District Name", AllowEmptyStrings = false)]
        public Int64 DistrictId { get; set; }
        [Required(ErrorMessage = "*Enter Hotel Branch Name in English", AllowEmptyStrings = false)]
        public string NameEn { get; set; }
        [Required(ErrorMessage = "*Enter Hotel Branch Name in Arabic", AllowEmptyStrings = false)]
        public string NameAr { get; set; }
        [Required(ErrorMessage = "*Enter Description of Hotel Branch in English", AllowEmptyStrings = false)]
        public string AboutEn { get; set; }
        [Required(ErrorMessage = "*Enter Description of Hotel Branch in Arabic", AllowEmptyStrings = false)]
        public string AboutAr { get; set; }
        [Required(ErrorMessage = "*Enter Phone Number", AllowEmptyStrings = false)]
        [DataType(DataType.PhoneNumber)]
        [Phone]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$", ErrorMessage = "Invalid Phone number")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "*Enter Email Id", AllowEmptyStrings = false)]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,5})+$",
        ErrorMessage = "Enter Correct Email Address")]
        public string Email { get; set; }
        [Required(ErrorMessage = "*Enter Landline Number", AllowEmptyStrings = false)]
        [DataType(DataType.PhoneNumber)]
        [Phone]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$", ErrorMessage = "Invalid Phone number")]
        public string Landline { get; set; }
        [Required(ErrorMessage = "*Enter Fax Number", AllowEmptyStrings = false)]
        [DataType(DataType.PhoneNumber)]
        [Phone]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$", ErrorMessage = "Invalid Phone number")]
        public string FaxNo { get; set; }
        [Required(ErrorMessage = "*Enter Hotel Branch Address in English", AllowEmptyStrings = false)]
        public string AddressEn { get; set; }
        [Required(ErrorMessage = "*Enter Hotel Branch Address in Arabic", AllowEmptyStrings = false)]
        public string AddressAr { get; set; }
        [Required(ErrorMessage = "*Enter Branch Hotel's Latitude", AllowEmptyStrings = false)]
        [RegularExpression(@"-?([1-9]?[0-9])\.{1}\d{1,6}", ErrorMessage = "Invalid Latitude")]
        public string Latitude { get; set; }
        [Required(ErrorMessage = "*Enter Branch Hotel's Longitude", AllowEmptyStrings = false)]
        [RegularExpression(@"-?([1-9]?[0-9])\.{1}\d{1,6}", ErrorMessage = "Invalid Longitude")]
        public string Longitude { get; set; }
        [Required(ErrorMessage ="*Enter Branch Booking Url",AllowEmptyStrings =false)]
        public string BookingUrl { get; set; }
        public string datasetXml { get; set; }
        public int BranchRating { get; set; }
        public string ServicesEn { get; set; }
        public string ServicesAr { get; set; }
        public string ImageUrl { get; set; }
        public string ImagePath { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public int DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
        public string AmenityIds { get; set; } 
        public IEnumerable<ManageBranchesDTO> BranchList { get; set; }
        public IEnumerable<ManageCitiesDTO> CitiesList { get; set; }
        public IEnumerable<ManageDistrictsDTO> DistrictsList { get; set; }
        public IEnumerable<ManageCountriesDTO> countriesList { get; set; }
        public IEnumerable<ManageHotelAmenitiesDTO> Amenities{ get; set; }
        public string message { get; set; }
        public int FlagId { get; set; }
        public string FacilityBgImagePath { get; set; }
    }
}