﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrairaHotel
{
    public class HotelBannersDTO
    {
        public Int64 Id { get; set; }
        public Int64 HotelId { get; set; }
        public string Language { get; set; }
        public Int64 MediaTypeIdId { get; set; }
        public string SourcePath { get; set; }
        public int SortIndex { get; set; }
        public string NavigationUrl { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public Int64 CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public Int64 ModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime DeletedOn { get; set; }
        public Int64 DeletedBy { get; set; }
    }
}