﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrairaHotel
{
    public class ManageOffersDTO
    {
        public Int64 Id { get; set; }
        public Int64 HotelId { get; set; }
        public Int64 BranchId { get; set; }
        public string HotelName { get; set; }
        public string OfferBookingUrl { get; set; }
        public bool BookingRedirection { get; set; }
        public string BookingUrl { get; set; }
        public int TypeId { get; set; }
        [Required(ErrorMessage = "*Enter Hotel Offer Name in English", AllowEmptyStrings = false)]
        [StringLength(20, ErrorMessage = "Hotel Offer Name cannot be more than 20 characters")]
        public string NameEn { get; set; }
        [Required(ErrorMessage = "*Enter Hotel Offer Name in Arabic", AllowEmptyStrings = false)]
        [StringLength(20, ErrorMessage = "Hotel Offer Name cannot be more than 20 characters")]
        public string NameAr { get; set; }
        [Required(ErrorMessage = "*Enter Hotel Offer Description in English", AllowEmptyStrings = false)]
        [StringLength(300, ErrorMessage = "Hotel offer Description cannot be more than 300 characters")]
        public string DescriptionEn { get; set; }
        [Required(ErrorMessage = "*Enter Hotel Offer Description in Arabic", AllowEmptyStrings = false)]
        [StringLength(300, ErrorMessage = "Hotel Offer Description cannot be more than 300 characters")]
        public string DescriptionAr { get; set; }
        [Required(ErrorMessage = "Please select file.")]
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif)$", ErrorMessage = "Only Image files allowed.")]
        public string ImageEn { get; set; }
        [Required(ErrorMessage = "Please select file.")]
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif)$", ErrorMessage = "Only Image files allowed.")]
        public string ImageAr { get; set; }
        //[DataType(DataType.Date),DisplayFormat(DataFormatString =@"{0:dd\/MM\/yyyy HH:mm",ApplyFormatInEditMode =true)]

        //[DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}")]

        [Required(ErrorMessage = "*Enter Start Date")]
        public string StartDateS { get; set; }
        [Required(ErrorMessage = "*Enter End Date")]
        public string EndDateE { get; set; }
        public DateTime? StartDate { get; set; }
        //[DataType(DataType.Date), DisplayFormat(DataFormatString = @"{0:dd\/MM\/yyyy HH:mm", ApplyFormatInEditMode = true)]
     
        public DateTime? EndDate { get; set; }
        public int SortIndex { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public int DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
        public IEnumerable<ManageOffersDTO> OffersList { get; set; }
        public string message { get; set; }
        public int FlagId { get; set; }
        public string datasetxml { get; set; }
        public string[] arAmnEn { get; set; }
        public string[] arAmnAr { get; set; }
        public string[] amIcons { get; set; }
        public decimal OfferPrice { get; internal set; }
        public int Mode { get; internal set; }
        public string Currency { get; internal set; }
    }
}