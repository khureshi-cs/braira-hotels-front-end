﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrairaHotel
{
    public class ManageBannersDTO
    {
        public long Id { get; set; }
        [Display(Name = "Hotel Name")]
        [Required(ErrorMessage = "*Please Select Hotel Name", AllowEmptyStrings = false)]
        public long HotelId { get; set; }
        [Required(ErrorMessage = "*Please Select Language", AllowEmptyStrings = false)]
        [StringLength(5, MinimumLength = 1, ErrorMessage = "*Max Length is 5")]
        public string Language { get; set; }
        [Required(ErrorMessage = "*Please Select Media Type", AllowEmptyStrings = false)]
        public long MediaTypId { get; set; }
        public string SourcePath { get; set; }
        public int SortIndex { get; set; }

        public bool isActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime DeletedOn { get; set; }
        public long ModifiedBy { get; set; }
        public long DeletedBy { get; set; }
        public bool isDeleted { get; set; }

        public IEnumerable<ManageBannersDTO> Hotellist { get; set; }
        public IEnumerable<ManageBannersDTO> Mediatypelist { get; set; }
        public IEnumerable<ManageBannersDTO> Bannerlist { get; set; }

        public string hotelname { get; set; }
        public string hotelbanner { get; set; }
        [Display(Name = "Media Type")]
        //[Required(ErrorMessage = "*Please Select MediaType")]
        public string mediatypename { get; set; }
        public string message { get; set; }
        public int FlagId { get; set; }
        public string datasetxml { get; set; }
        public string ContentType { get; set; }
        public string filepath { get; set; }
        public string Hotel { get; set; }
        public bool fileuploadvalue{get;set;}


    }
}