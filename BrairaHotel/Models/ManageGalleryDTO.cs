﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrairaHotel
{
    public class ManageGalleryDTO
    {
        public Int64 Id { get; set; }
        [Required(ErrorMessage ="Select Hotel Name",AllowEmptyStrings =false)]
        public Int64 HotelId { get; set; }
        [Required(ErrorMessage = "Select Country Name", AllowEmptyStrings = false)]
        public Int64 CountryId { get; set; }
        [Required(ErrorMessage = "Select City Name", AllowEmptyStrings = false)]
        public Int64 CityId { get; set; }
        //[Required(ErrorMessage = "Select District Name", AllowEmptyStrings = false)]
        //public Int64 DistrictId { get; set; }
        [Required(ErrorMessage = "Select Branch Name", AllowEmptyStrings = false)]
        public Int64 BranchId { get; set; }
        [Required(ErrorMessage = "Select Gallery Name", AllowEmptyStrings = false)]
        public Int64 GalleryCatId { get; set; }
        [Required(ErrorMessage = "Select Media Type Name", AllowEmptyStrings = false)]
        public Int64 MediaTypeId { get; set; }
        [Required(ErrorMessage = "Select Image", AllowEmptyStrings = false)]
        public string ImagePath { get; set; }
        public string HotelName { get; set; }
        public string CountryName { get; set; }
        public string CityName { get; set; }
        public string BranchName { get; set; }
        public string GalleryCategeryName { get; set; }
        public string MediaType { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public int DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
        public string message { get; set; }
        public string datasetxml { get; set; }
        public int FlagId { get; set; }

        public string BranchNameEn { get; set; }
        public string BranchNameAr { get; set; }
        public string ThumbnailImage { get; set; }

        public IEnumerable<ManageGalleryDTO> GalleryList { get; set; }
    }
}