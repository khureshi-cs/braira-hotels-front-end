﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrairaHotel.Models
{
    public class FindAHotelNewDTO
    {
        public Int64 Id { get; set; }
        public Int64 HotelId { get; set; }
        public string HotelNameEn { get; set; }
        public string HotelNameAr { get; set; }
        public string AddressEn { get; set; }
        public string AddressAr { get; set; }
        public string BranchNameEn { get; set; }
        public string BranchNameAr { get; set; }
        public string CountryEn { get; set; }
        public Int64 CountryId { get; set; }
        public string CountryAr { get; set; }
        public string CityEn { get; set; }
        public string CityAr { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public string BookingURL { get; set; }
        public string slug { get; set; }
        public string AmenityIds { get; set; }
        public long CityId { get; internal set; }
    }
}