﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BrairaHotel
{
    public class ManageAboutUsDTO
    {
        public long Id { get; set; }
        public long HotelId { get; set; }
        [Display(Name = "Content (English)")]
        [Required(ErrorMessage = "*Please Enter Content (English)", AllowEmptyStrings = false)]
        public string ContentEn { get; set; }
        [Display(Name = "Content (Arabic)")]
        [Required(ErrorMessage = "*Please Enter Content (Arabic)", AllowEmptyStrings = false)]
        public string ContentAr { get; set; }
        public string ImageEn { get; set; }
        public string ImageAr { get; set; }

        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public long ModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime DeletedOn { get; set; }
        public long DeletedBy { get; set; }
        public IEnumerable<ManageAboutUsDTO> aboutuslist { get; set; }
        public string message { get; set; }
        public int FlagId { get; set; }
        public string datasetxml { get; set; }
        public string hotelname { get; set; }
        public string ContentType { get; set; }
        public string Hotel { get; set; }


    }
}