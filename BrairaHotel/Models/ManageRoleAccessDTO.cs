﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrairaHotel
{
    public class ManageRoleAccessDTO
    {
        public Int64 Id { get; set; }
        public Int64 RoleId { get; set; }
        public Int64 MenuItemId { get; set; }
        public bool isSelected { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public int DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
        public string message { get; set; }
        public int FlagId { get; set; }
        public IEnumerable<ManageRoleAccessDTO> RoleAccessList { get; set; }
        public string MenuItemIds { get; set; }
        public IEnumerable<MenuItemsDTO> MenuItemlist { get; set; }
        public IEnumerable<string> MenuCategory { get; set; }
        public IEnumerable<string> MenuItems { get; set; }
    }
}