﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrairaHotel
{
    public class ManageUserAccessDTO
    {
        public Int64 Id{ get; set; }
        public Int64 UserId { get; set; }
        public string FullName { get; set; }
        public Int64 HotelId { get; set; }
        public string Hotelname { get; set; }
        public Int64 RoleId { get; set; }
        public string name { get; set; }
        public Int64 CountryId { get; set; }
        public string CountryName { get; set; }
        public Int64 CityId { get; set; }
        public string CityName { get; set; }
        public Int64 DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string datasetxml { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public Int64 CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public Int64 ModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime DeletedOn { get; set; }
        public Int64 DeletedBy { get; set; }
        public string message { get; set; }
        public int FlagId { get; set; }
        public IEnumerable<ManageUserAccessDTO> UserAccessList { get; set; }
    }
}