﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace BrairaHotel
{
    public class PNMessagesDTO
    {
        public Int64 Id { get; set; }
        [Display(Name = "Message (English)")]
        [Required(ErrorMessage = "*Please Enter Message (English)", AllowEmptyStrings = false)]
        [StringLength(200, MinimumLength = 1, ErrorMessage = "*Max Length is 200")]
        public string MessageEn { get; set; }
        [Display(Name = "Message (Arabic)")]
        [Required(ErrorMessage = "*Please Enter Message (Arabic)", AllowEmptyStrings = false)]
        [StringLength(200, MinimumLength = 1, ErrorMessage = "*Max Length is 200")]
        public string MessageAr { get; set; }
        public bool IsActive { get; set; }
        public bool IsSent { get; set; }
        public DateTime CreatedOn { get; set; }
        public int Createdby { get; set; }

        public int FlagId { get; set; }
        public string message { get; set; }

        public IEnumerable<PNMessagesDTO> PNMessageslist { get; set; }

    }
}