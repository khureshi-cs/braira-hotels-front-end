﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace BrairaHotel
{
    public class ManageCareersDTO
    {
        public Int64 Id { get; set; }
        [Display(Name ="Country Name")]
        public Int64 CountryId { get; set; }
        [Display(Name ="Hotel Name")]
        public Int64 HotelId { get; set; }
        public Int64 CityId { get; set; }
        public Int64 DistrictId { get; set; }
        public Int64 JobCategoryId { get; set; }
        public string PositionEn { get; set; }
        public string PositionAr { get; set; }
        public string DescEn { get; set; }
        public string DescAr { get; set; }

        public bool isActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long Createdby { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime DeletedOn { get; set; }
        public long ModifiedBy { get; set; }
        public long DeletedBy { get; set; }
        public bool isDeleted { get; set; }

        public string message { get; set; }
        public int FlagId { get; set; }
        public string datasetxml { get; set; }
        public string hname { get; set; }
        public IEnumerable<ManageCareersDTO> careerslist { get; set; }
        public string cityname { get; set; }
        public string hotelname { get; set; }
        public string countryname { get; set; }
        [Display(Name = "Hotel Name")]
        [Required(ErrorMessage = "*Please Select Hotel", AllowEmptyStrings = false)]
        public IEnumerable<ManageCareersDTO> HotelsList { get; set; }


    }
}