﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrairaHotel
{
    public class ManageUsersDTO
    {
        public Int64 Id { get; set; }
        [Required(ErrorMessage ="*Enter Employee Id",AllowEmptyStrings =false)]
        public Int64? EmpId { get; set; }
        [Required(ErrorMessage ="*Enter Employee Full Name",AllowEmptyStrings =false)]
        public string FullName { get; set; }
        [Required(ErrorMessage ="*Enter Email Id",AllowEmptyStrings =false)]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$",
        ErrorMessage = "Enter Correct Email Address")]
        public string Email { get; set; }
        [Required(ErrorMessage ="*Enter Mobile Number",AllowEmptyStrings =false)]
        [DataType(DataType.PhoneNumber)]
        [Phone]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$", ErrorMessage = "Invalid Phone number")]
        public string Mobile { get; set; }
        [Required(ErrorMessage ="*Select Hotel Name",AllowEmptyStrings =false)]
        public Int64 MasterHotelId { get; set; }
        [Required(ErrorMessage ="*Enter User Name",AllowEmptyStrings =false)]
        public string UserName { get; set; }
        [Required(ErrorMessage ="*Enter Password",AllowEmptyStrings =false)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required(ErrorMessage = "Confirmation Password is required.")]
        [NotMapped]
        [Compare("Password", ErrorMessage = "Password and Confirmation Password must match.")]
        public string ConfirmPassword { get; set; }
        public int RoleId { get; set; }
        public string Name { get; set; }
        public bool isActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public int DeletedBy { get; set; }
        public bool isDeleted { get; set; }
        public IEnumerable<ManageUsersDTO> Userslist { get; set; }
        public IEnumerable<CountryList> CountrList { get; set; }
        public string message { get; set; }
        public int FlagId { get; set; }
        public string Roles { get; set; }
    }
    public class CountryList
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
    }


}