﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BrairaHotel
{
    public class ManageHotelsDTO
    {
        public Int64 Id { get; set; }
        [Required(ErrorMessage ="*Enter Hotel Name in English",AllowEmptyStrings =false)]
        [StringLength(20,ErrorMessage ="Hotel Name cannot be more than 20 characters")]
        public string NameEn { get; set; }
        [Required(ErrorMessage ="*Enter Hotel Name in Arabic",AllowEmptyStrings =false)]
        [StringLength(20, ErrorMessage = "Hotel Name cannot be more than 20 characters")]
        public string NameAr { get; set; }
        [Required(ErrorMessage ="*Enter Hotel Description in English",AllowEmptyStrings =false)]
        [AllowHtml]
        public string DescEn { get; set; }
        [Required(ErrorMessage ="*Enter Hotel Description in Arabic",AllowEmptyStrings =false)]
        [AllowHtml]
        public string DescAr { get; set; }
        [Required(ErrorMessage ="*Upload Hotel logo in English",AllowEmptyStrings =false)]
        public string LogoEn { get; set; }
        [Required(ErrorMessage ="*Upload Hotel logo in Arabic",AllowEmptyStrings =false)]
        public string LogoAr { get; set; }
        [Required(ErrorMessage ="*Select Hotel Type Group/Hotel",AllowEmptyStrings =false)]
        public int Type { get; set; }
        [Required(ErrorMessage = "*Select Country", AllowEmptyStrings = false)]
        public Int64 CountryId { get; set; }
        public Int64 OldCountryId { get; set; }
        public Int64 ParentId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public int DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
        public string CountryIds { get; set; }
        public long[] aaa { get; set; }
        public IEnumerable<ManageCountriesDTO> CountryList { get; set; }
        public IEnumerable<ManageHotelsDTO> HotelsList { get; set; }
        public string message { get; set; }
        public int FlagId { get; set; }
    }
}