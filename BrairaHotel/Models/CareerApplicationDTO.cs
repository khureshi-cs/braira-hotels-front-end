﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrairaHotel
{
    public class CareerApplicationDTO
    {
        public Int64 Id { get; set; }
        [Required]
        public Int64 HotelId { get; set; }
        [Required]
        public Int64 CityId { get; set; }
        [Required]
        public string FullName { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public Int64 JobCategoryId { get; set; }
        [Required]
        public Int64 PositionId { get; set; }
        public string PositionEn { get; set; }
        public string PositionAr { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public string ProfilePath { get; set; }
        public string datasetxml { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long Createdby { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime DeletedOn { get; set; }
        public long ModifiedBy { get; set; }
        public long DeletedBy { get; set; }
        public bool isDeleted { get; set; }
        public string message { get; set; }
    }
}