﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrairaHotel
{
    public class ContactRequestDTO
    {
        public int Id { get; set; }
        public Int64 HotelId { get; set; }
        [Required(ErrorMessage = "*Enter Full Name", AllowEmptyStrings = false)]
        public string FullName { get; set; }
        [Required(ErrorMessage = "*Enter Email Id", AllowEmptyStrings = false)]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$",
        ErrorMessage = "Enter Correct Email Address")]
        public string Email { get; set; }
        [Required(ErrorMessage = "*Enter Mobile Number", AllowEmptyStrings = false)]
        [DataType(DataType.PhoneNumber)]
        [Phone]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$", ErrorMessage = "Invalid Phone number")]
        public string Phone { get; set; }
        public string Message { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime DeletedOn { get; set; }
        public long ModifiedBy { get; set; }
        public long DeletedBy { get; set; }
        public bool isDeleted { get; set; }
        public string MsgStatus { get; set; }
    }
}