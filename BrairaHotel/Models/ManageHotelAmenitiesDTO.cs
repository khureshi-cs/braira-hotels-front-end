﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BrairaHotel
{
    public class ManageHotelAmenitiesDTO
    {
        public long Id { get; set; }
        [Display(Name = "Name (English)")]
        [Required(ErrorMessage = "*Please Enter Name (English)", AllowEmptyStrings = false)]
        public string NameEn { get; set; }
        [Display(Name = "Name (Arabic)")]
        [Required(ErrorMessage = "*Please Enter Name (Arabic)", AllowEmptyStrings = false)]
        public string NameAr { get; set; }
        public string IconClass { get; set; }
        [Display(Name = "Icon")]
      //  [Required(ErrorMessage = "*Please Upload Icon", AllowEmptyStrings = false)]
        public string IconImage { get; set; }

        public bool isSelected { get; set; }
        public bool isActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime DeletedOn { get; set; }
        public long ModifiedBy { get; set; }
        public long DeletedBy { get; set; }
        public bool isDeleted { get; set; }

        public string message { get; set; }
        public int FlagId { get; set; }
        public string datasetxml { get; set; }
        public List<ManageHotelAmenitiesDTO> ManageHotelAmenitiesList { get; set; }
        public string imagepath { get; set; }
        public string ContentType { get; set; }
        public string filepath { get; set; }

       

    }
}