﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrairaHotel
{
    public class ManageRolesDTO
    {
        public Int64 Id { get; set; }

        [Required(ErrorMessage = "*Please Enter Role Name", AllowEmptyStrings = false)]
        [StringLength(25, MinimumLength = 1, ErrorMessage = "Max Length is 25")]
        public string RoleName { get; set; }
        public bool isSuperAdmin { get; set; }
        public bool isActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime DeletedOn { get; set; }
        public int ModifiedBy { get; set; }
        public int DeletedBy { get; set; }
        public int isDeleted { get; set; }
        public IEnumerable<ManageRolesDTO> Roleslist { get; set; }
        public string message { get; set; }
        public int FlagId { get; set; }
    }
}