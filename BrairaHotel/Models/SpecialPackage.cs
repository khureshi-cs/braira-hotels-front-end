﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrairaHotel
{
    public class SpecialPackage
    {
        public Int64 Id { get; set; }
        public Int64 HotelId { get; set; }
        public Int64 BranchId { get; set; }
        public string bookingURL { get; set; }

        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public string DescriptionEn { get; set; }
        public string DescriptionAr { get; set; }

        public string ImageEn { get; set; }
        public string ImageAr { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public Int32 SortIndex { get; set; }
        public bool BookingRedirection { get;set; }
        public string AmenitiesEn { get; set; }
        public string AmenitiesAr { get; set; }
        public string IconClass { get; set; }
        public Int32 OfferType { get; set; }
        public Int32 Mode { get; set; }
        public decimal OfferPrice { get; set; }
        public String Currency { get; set; }

        public string[] arAmnEn { get; set; }
        public string[] arAmnAr { get; set; }
        public string[] amIcons { get; set; }

    }
}