﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DataAccessLayer
{
    public static class DBEngine
    {
        #region Connection Methods
        #region Static Connection Object
        private static SqlConnection _connection;
        #endregion

        #region Default Constructor to Initialize Connection Object
        static DBEngine()
        {
            _connection = new SqlConnection(ApplicationConfiguration.DbConnectionString);

        }
        #endregion


        #region Open Connection
        public static void OpenConnection()
        {
            if (!_connection.State.Equals(ConnectionState.Open))
                _connection.Open();
        }

        #endregion

        #region Close Connection
        public static void CloseConnection()
        {
            if (!_connection.State.Equals(ConnectionState.Closed))
                _connection.Close();
        }

        #endregion
        #endregion

        #region Execute Scalar
        /// <summary>
        /// Executes Scalar Methods and returns a variable data type object which can be converted into any standard data type
        /// </summary>
        /// <param name="command">SQL Command with or without parameters which needs to be executed</param>
        /// <returns>Variable of data type object which can be converted into any standard data type</returns>
        public static object ExecuteScalar(SqlCommand command)
        {
            object objResult = new object();
            try
            {
                command.Connection = _connection;
                command.CommandType = CommandType.StoredProcedure;
                OpenConnection();
                objResult = command.ExecuteScalar();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
                command.Dispose();
            }
            return objResult;
        }
        #endregion

        #region Execute DataSet
        /// <summary>
        /// Executes DataSet and returns DataSet filled with results of stored procedure
        /// </summary>
        /// <param name="command">SQL Command with or without parameters which needs to be executed</param>
        /// <returns>DataSet filled with results of stored procedure</returns>
        public static DataSet ExecuteDataSet(SqlCommand command)
        {
            try
            {
                DataSet _ds = new DataSet();
                command.Connection = _connection;
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter _da = new SqlDataAdapter(command);
                _da.Fill(_ds);
                return _ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                command.Dispose();
            }
        }
        #endregion

        #region Execute DataTable
        /// <summary>
        /// Executes DataTable and returns DataTable filled with results of stored procedure
        /// </summary>
        /// <param name="command">SQL Command with or without parameters which needs to be executed</param>
        /// <returns>DataTable filled with results of stored procedure</returns>
        public static DataTable ExecuteDataTable(SqlCommand command)
        {
            try
            {
                DataTable _dt = new DataTable();
                command.Connection = _connection;
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter _da = new SqlDataAdapter(command);
                _da.Fill(_dt);
                return _dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                command.Dispose();
            }
        }

        #endregion

        #region Execute Non-Query
        /// <summary>
        /// Executes NonQuery and returns number of rows affected
        /// </summary>
        /// <param name="command">SQL Command with or without parameters which needs to be executed</param>
        /// <returns>Number of rows affected</returns>
        public static int ExecuteNonQuery(SqlCommand command)
        {
            int rowsAffected = 0;
            try
            {
                command.Connection = _connection;
                command.CommandType = CommandType.StoredProcedure;
                OpenConnection();
                rowsAffected = command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
                command.Dispose();
            }
            return rowsAffected;
        }
        #endregion
    }
}
