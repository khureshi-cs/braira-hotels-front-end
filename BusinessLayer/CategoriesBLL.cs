﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertiesLayer;
using DataAccessLayer;
using System.Data.SqlClient;

namespace BusinessLayer
{
    public class CategoriesBLL
    {
        public static bool SaveCategory(PropertiesLayer.Category _category)
        {
            SqlCommand _command = new SqlCommand("Admin.SaveCategory");

            if (_category.CategoryId > 0)
                _command.Parameters.AddWithValue("@CategoryId", _category.CategoryId);

            _command.Parameters.AddWithValue("@CategoryName", _category.CategoryName);
            _command.Parameters.AddWithValue("@CategoryName_Ar", _category.CategoryName_Ar);
            _command.Parameters.AddWithValue("@UserId", _category.UserId);

            bool? isActive = true;
            if (_category.IsActive != null)
                isActive = _category.IsActive;

            _command.Parameters.AddWithValue("@IsActive", isActive);

            return (DBEngine.ExecuteNonQuery(_command) > 0);
        }
    }
}
